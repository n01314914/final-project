﻿<%@ Page Title="page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="page.aspx.cs" Inherits="final_project.page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h3 id="page_name" runat="server"></h3>
  
    <asp:SqlDataSource 
        runat="server"
        id="page_del"
        ConnectionString="<%$ ConnectionStrings:final_sql_con %>">
    </asp:SqlDataSource>
    <asp:Button runat="server" id="page_del_btn"
        OnClick="Page_Del"
        OnClientClick="if(!confirm('Are you sure?')) return false;"
        Text="Delete" />
    <div id="delDebug" class="querybox" runat="server"></div>
    <a href="update_page.aspx?pageid=<%Response.Write(this.pageid);%>">Edit</a>

   <asp:SqlDataSource runat="server"
        id="select_page"
        ConnectionString="<%$ ConnectionStrings:final_sql_con %>">
    </asp:SqlDataSource>
       
    <div id="query_page" runat="server" class="querybox">
    </div>
    <asp:DataGrid ID="page_list" runat="server" CssClass="table">
    </asp:DataGrid>
</asp:Content>
