﻿
<%@ Page Title="new page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="new_page.aspx.cs" Inherits="final_project.new_page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<h3>New Page</h3>
    <asp:SqlDataSource runat="server" id="add_page"
        ConnectionString="<%$ ConnectionStrings:final_sql_con %>">

    </asp:SqlDataSource>

    <div class="inputrow">
        <label>Page title:</label>
        <asp:TextBox ID="page_title" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="page_title"
            ErrorMessage="Enter a page name">
        </asp:RequiredFieldValidator>
    </div>
    
    <div class="inputrow">
        <label>Page Content:</label>
        <asp:TextBox runat="server" ID="pagecontent"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ID="page_content_validate" controlToValidate="pagecontent" ErrorMessage="Enter Page content "></asp:RequiredFieldValidator>
    </div>
    
     <div class="inputrow">
        <label>Page Content2:</label>
        <asp:TextBox runat="server" ID="page_content2"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ID="page_content2_validate" controlToValidate="page_content2"  ErrorMessage="Enter Page content2 "></asp:RequiredFieldValidator>
    </div>

     <div class="inputrow">
        <label>Author:</label>
        <asp:TextBox runat="server" ID="author_name"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ID="authorName_Validator" controlToValidate="author_name"  ErrorMessage="Enter Author name "></asp:RequiredFieldValidator>
    </div>
    
    <ASP:Button Text="Add Page" runat="server" OnClick="newPage"/>

    <div runat="server" id="debug" class="querybox"></div>
</asp:Content>

