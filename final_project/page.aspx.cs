﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace final_project
{
    public partial class page : System.Web.UI.Page
    {
        
        public string pageid
        {
            get { return Request.QueryString["pageid"]; }
        }
        private string page_query =
            "SELECT pageid,pagetitle,pagecontent,pagecontent2,author from pages";

        protected void Page_Load(object sender, EventArgs e)
        {

            if (pageid == "" || pageid == null) page_name.InnerHtml = "page not found.";
            else
            {
                page_query += " WHERE PAGEID = " + pageid;
                
                select_page.SelectCommand = page_query;
                query_page.InnerHtml = page_query;

               
                DataView view_page = (DataView)select_page.Select(DataSourceSelectArguments.Empty);
                
                DataRowView page_row_view = view_page[0];
                string page_title = view_page[0]["pagetitle"].ToString();
                page_name.InnerHtml = page_title;

                page_list.DataSource = page_Manual_Bind(select_page);
                page_list.DataBind();

                
            }
        }
        protected DataView page_Manual_Bind(SqlDataSource src)
        {
            

            DataTable mypagetbl;
            DataView mypageview;
            mypagetbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            foreach (DataRow row in mypagetbl.Rows)
            {
                
                row["pagetitle"] =
                    "<a href=\"page.aspx?pageid="
                    + row["pageid"]
                    + "\">"
                    + row["pagetitle"]
                    + "</a>";

            }
            mypagetbl.Columns.Remove("pageid");
            mypageview = mypagetbl.DefaultView;

            return mypageview;
        }


       
        protected void Page_Del(object sender, EventArgs e)
        {
            
            string delquery = "DELETE FROM pages WHERE pageid=" + pageid;

            delDebug.InnerHtml = delquery;
            page_del.DeleteCommand = delquery;
            page_del.Delete();

        }
    }
}