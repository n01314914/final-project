﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace final_project
{
    public partial class pages : Page
    {
        private string basequery = "SELECT pageid,pagetitle,pagecontent,pagecontent2,author FROM pages";

        protected void Page_Load(object sender, EventArgs e)
        {
            pages_select.SelectCommand = basequery;
            
            list_pages.DataSource = page_Manual_Bind(pages_select);
            query_pages.InnerHtml = basequery;
            list_pages.DataBind();
        }

        protected DataView page_Manual_Bind(SqlDataSource src)
        {
            
            DataTable mypagetbl;
            DataView mypageview;
            mypagetbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            foreach (DataRow row in mypagetbl.Rows)
            {
                
                row["pagetitle"] =
                    "<a href=\"page.aspx?pageid="
                    + row["pageid"]
                    + "\">"
                    + row["pagetitle"]
                    + "</a>";
            }
            mypagetbl.Columns.Remove("pageid");

            mypageview = mypagetbl.DefaultView;

            return mypageview;

        }
    }
}