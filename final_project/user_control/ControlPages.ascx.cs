﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace final_project.user_control
{
    public partial class ControlPages : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string query = "select * from pages";
            select_page.SelectCommand = query;
            DataView pagerows = (DataView)select_page.Select(DataSourceSelectArguments.Empty);
            string menulist = "";
            foreach (DataRow pagerow in pagerows.ToTable().Rows)
            {
                string pagetitle = pagerow["pagetitle"].ToString();
                string pageid = pagerow["pageid"].ToString();
                menulist +="<li><a href=\"page.aspx?pageid="+pageid+"\">" +pagetitle+ "</a></li>";


            }
            menu_list.InnerHtml = menulist;
        }
    }
}