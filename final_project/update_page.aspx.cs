﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace final_project
{
    public partial class update_page : System.Web.UI.Page
    {
        public int pageid
        {
            get { return Convert.ToInt32(Request.QueryString["pageid"]); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Page_Edit()
        {

        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            
            DataRowView pagerow = getpageInfo(pageid);
            if (pagerow == null)
            {
                pagename.InnerHtml = "No page Found.";
                return;
            }
            page_title.Text = pagerow["pagetitle"].ToString();

            page_content.Text = pagerow["pagecontent"].ToString();

            page_content2.Text = pagerow["pagecontent2"].ToString();

            author_name.Text = pagerow["author"].ToString();

        }

        protected void Page_Edit(object sender, EventArgs e)
        {

            string P_title = page_title.Text;
            string p_content = page_content.Text;
            string p_content2 = page_content2.Text;
            string P_author = author_name.Text;
           


            string editquery = "Update pages set pagetitle='" + P_title + "'," +
                " pagecontent='"  + p_content + "'," +
                " pagecontent2='" + p_content2 + "'," +
                " author='" + P_author + "'" +
                "where pageid=" + pageid;

            debug.InnerHtml = editquery;

            page_edit.UpdateCommand = editquery;
            page_edit.Update();


        }

        protected DataRowView getpageInfo(int id)
        {
            string query = "select * from pages where pageid=" + pageid.ToString();
            select_page.SelectCommand = query;
            
            DataView pageview = (DataView)select_page.Select(DataSourceSelectArguments.Empty);

           
            if (pageview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView pagerowview = pageview[0];
            return pagerowview;

        }
    }
}