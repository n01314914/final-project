﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManagePages.aspx.cs" Inherits="final_project.ManagePages" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <asp:SqlDataSource ID="pages_select" selectcommand="select * from pages;" runat="server" ConnectionString="<%$ connectionStrings:final_sql_con %>"></asp:SqlDataSource>
    <asp:DataGrid ID="pages_list" runat="server" DataSourceID="pages_select"></asp:DataGrid>   
    </asp:Content>
