﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="update_page.aspx.cs" Inherits="final_project.update_page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
    <h3 runat="server" id="pagename">Edit page</h3>
    <asp:SqlDataSource runat="server" id="page_edit"
        ConnectionString="<%$ ConnectionStrings:final_sql_con %>">

    </asp:SqlDataSource>
    <asp:SqlDataSource runat="server" id="select_page"
        ConnectionString="<%$ ConnectionStrings:final_sql_con %>">
    </asp:SqlDataSource>

    <div class="inputrow">
        <label>Page Title:</label>
        <asp:TextBox ID="page_title" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ID="title_validate"
            ControlToValidate="page_title"
            ErrorMessage="Enter a page title">
        </asp:RequiredFieldValidator>
    </div>
   
    
    <div class="inputrow">
        <label>Page Content:</label>
        <asp:TextBox runat="server" ID="page_content"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ID="content_validate" controlToValidate="page_content" ErrorMessage="Enter a page content"></asp:RequiredFieldValidator>
    </div>


     <div class="inputrow">
        <label>Page Content2:</label>
        <asp:TextBox runat="server" ID="page_content2"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ID="pagecontent2_Validator1" controlToValidate="page_content2" ErrorMessage="Enter a page content2"></asp:RequiredFieldValidator>
    </div>

    <div class="inputrow">
        <label>Author:</label>
        <asp:TextBox runat="server" ID="author_name"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" controlToValidate="author_name" ErrorMessage="Enter author name"></asp:RequiredFieldValidator>
    </div>
   
    <ASP:Button Text=" Page Edit" runat="server" OnClick="Page_Edit"/>

    <div runat="server" id="debug" class="querybox"></div>
    
</asp:Content>
