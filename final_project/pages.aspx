﻿<%@ Page Title="Pagess" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="pages.aspx.cs" Inherits="final_project.pages" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>All Pages</h2>
   
    <a href="new_page.aspx">New Page</a>
   
    <asp:SqlDataSource runat="server"
        id="pages_select"
       
        ConnectionString="<%$ ConnectionStrings:final_sql_con %>">
    </asp:SqlDataSource>
    <div id="query_pages" class="querybox" runat="server">
      
    </div>
    <asp:DataGrid id="list_pages"
        runat="server" CssClass="table">
    </asp:DataGrid>

    <div id="debug" runat="server"></div>
</asp:Content>
